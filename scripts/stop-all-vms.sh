#!/bin/bash

# Define array of container name suffixes
suffixes=("-olip-api" "-olip-dashboard" "-hao")

# Get IDs of all Docker containers
container_ids=$(docker ps -q)

# Iterate through each container ID
for container_id in $container_ids; do
    # Get the container name
    container_name=$(docker inspect -f '{{.Name}}' $container_id)

    # Remove the leading '/' character
    container_name=${container_name#/}

    # Check if the container name ends with any of the defined suffixes
    for suffix in "${suffixes[@]}"; do
        if [[ "$container_name" == *"$suffix" ]]; then
            echo "Stopping container: $container_name"
            # Stop the container
            docker stop $container_id
		    docker rm $container_id
            break  # No need to check further suffixes if one match is found
        fi
    done
done

echo "Done."
echo "VM's applications are not stopped by this script."
