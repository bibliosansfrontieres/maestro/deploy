#!/bin/bash

VM_ID=$1

if [ -z "$VM_ID" ]; then
    echo "Usage: $0 <vmId>"
    echo "Please provide the vmId."
    exit 1
fi

echo "Removing vmId: $VM_ID..."

container_ids=$(docker ps -q)

# Iterate through each container ID
for container_id in $container_ids; do
    # Get the container name
    container_name=$(docker inspect -f '{{.Name}}' $container_id)

    # Remove the leading '/' character
    container_name=${container_name#/}

    # Check if the container name starts with vmId
    if [[ "$container_name" == "$VM_ID"* ]]; then
        echo "Stopping container: $container_name"
        # Stop the container
        docker stop $container_id
	docker rm $container_id
    fi
done

echo "Done."
echo "VM's applications are not stopped by this script."
