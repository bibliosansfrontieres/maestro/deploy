#!/bin/bash

# Check if branch name argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <branch_name>"
    exit 1
fi

BRANCH_NAME=$1

# Remplacer les / par des -
BRANCH_NAME_CLEAN=$(echo "$BRANCH_NAME" | sed 's/\//-/g')

BRANCH_FOLDER="../branches/$BRANCH_NAME_CLEAN"
BRANCH_URL="$BRANCH_NAME_CLEAN.staging.maestro.bsf-intranet.org"
DASHBOARD_URL="https:\/\/$BRANCH_URL"

# Create branch folder if it doesn't exist
if [ ! -d "$BRANCH_FOLDER" ]; then
    echo "Create folder"
    mkdir -p "$BRANCH_FOLDER"
fi

cd "$BRANCH_FOLDER"

cp ../.env ./.env
sed -i "s/^GITLAB_BRANCH=.*/GITLAB_BRANCH=$BRANCH_NAME_CLEAN/" ".env"
sed -i "s/^MAESTRO_HOST=.*/MAESTRO_HOST=$BRANCH_URL/" ".env"

cp ../.maestro-dashboard.env ./.maestro-dashboard.env
sed -i "s/^PUBLIC_DASHBOARD_URL=.*/PUBLIC_DASHBOARD_URL=$DASHBOARD_URL/" ".maestro-dashboard.env"
sed -i "s/^PUBLIC_API_URL=.*/PUBLIC_API_URL=https:\/\/api.staging.maestro.bsf-intranet.org/" ".maestro-dashboard.env"
sed -i "s/^PUBLIC_BASE_URL=.*/PUBLIC_BASE_URL=staging.maestro.bsf-intranet.org/" ".maestro-dashboard.env"
sed -i "s/^PUBLIC_DEBUG=.*/PUBLIC_DEBUG=true/" ".maestro-dashboard.env"

cp ../compose.yml ./compose.yml

docker compose pull
docker compose up -d --force-recreate

echo "Branch $BRANCH_NAME updated successfully."
echo "URL of deploy : https://$BRANCH_URL"