# Maestro deploy

## Procedures

### Restart Maestro API

```shell
cd ~/deploy && docker compose up -d --force-recreate maestro-api
```

### Restart stack

```shell
cd ~/deploy && docker compose up -d --force-recreate
```

### Recreate Maestro

**WARNING ! Only for staging, never for production**

```shell
cd ~/deploy && docker compose down
cd ~/deploy/scripts && sh stop-all-vms.sh
sudo rm -rf ~/maestro_volume
docker volume rm elasticsearch_volume
cd ~/deploy && docker compose up -d --force-recreate
```

## Installation on Debian Server

### Get docker

```shell
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

### Install s3fs

```shell
sudo apt install s3fs
```

### Clone this repository

```shell
git clone https://gitlab.com/bibliosansfrontieres/maestro/deploy.git
cd deploy
```

### Mount s3 volume

```shell
cp maestro-contents-credentials.example maestro-contents-credentials
nano maestro-contents-credentials
```

You need now to put S3 credentials

```shell
chmod 600 maestro-contents-credentials
sudo nano /etc/fuse.conf
```

Uncomment this line : `#user_allow_other`

Mount volume :

```shell
mkdir ../s3
s3fs maestro-contents ../s3 -o passwd_file=./maestro-contents-credentials -o url=https://s3.eu-west-2.wasabisys.com -o allow_other
```

### Configure env files

Copy all env files :

```shell
cp .env.example .env
cp .maestro-api.env.example .maestro-api.env
cp .maestro-dashboard.env.example .maestro-dashboard.env
```

Configure thoses files.

Run docker compose :

```shell
docker compose up -d
```

### Todo

Explain each service configuration

#### S3

#### Entra

#### Omeka

#### Gitlab

#### Gandi
